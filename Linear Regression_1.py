import numpy as np
from sklearn.linear_model import LinearRegression
X = np.array([[1, 1], [1, 2], [2, 2], [2, 3]])
import matplotlib.pyplot as plt
'''
Linear Regression One Feature
'''
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''
##################### 1 a #####################
X = np.array([[1], [2], [3], [4]])#array with 4 rows and one column, one feature
x_0 = X[:]
y = 1 * x_0 + 3
# in the plot, the first argument is the values on the x axis, the second is the column
# from the array:
plt.plot(x_0, y)

reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("1 a:")
print("4 rows (m=4), 1 columns/features (n=1)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)

'''
since in each row  y is a linear equation of X without noise
reg.coef_ and reg.intercept are the same as the coefficients used when creating y
'''
print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# predict the value of a new point:
y_predicted_new_point = reg.predict(np.array([[3]]))
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

# predict on the same data the model was trained on:
y_predicted_on_training_data = reg.predict(X)
'''
since in each row  y is a linear equation of X without noise
y_true and y_hat are the same
'''
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
plt.close() # close last open plot
plt.scatter(X,  y, label = "real y") # ravel flattens X to 1D
'''
add line to figure, f'' and f"" are called f strings/ formatted strings in Python 3
and are useful for printing the dynamic content of a variable together with static text content
'''
plt.plot(X,  y_predicted_on_training_data, label = f'predicted y score {score} intercept {reg.intercept_} coefficients {reg.coef_}')
plt.legend(loc="upper right")#show labels of lines
#plt.tight_layout()
plt.savefig('1a_y_real_n_predicted.png')
#######################################################################
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''
##################### 1 b  - add noise to y #####################
'''
repeat [1,2] as a tile (אריח) two times
useful when you want to your data to have a repeating pattern
'''
X = np.tile([1, 2], 2).reshape(-1, 1)
'''
in the second run - comment the following line out and see an error later on
since we need a 2D array for X:
'''
X = X.reshape(-1, 1)
# change the value of rows 0 and 1 to the minus the
# value of the last two rows
X[:2] = -1 * X[-2:]
x_0 = X[:]
y = 1 * x_0 + 3

"""
 create an array with the same shape as y, with random floats between 0 and 1
 adds noise to y 
"""
noise = np.random.random((4,1))
y = y + noise
reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 1 columns/features (n=1)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)
print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# change the predicted new sample to one feature
y_predicted_new_point = reg.predict(np.array([[3]]))        # predict the value of a new point
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

y_predicted_on_training_data = reg.predict(X)  # predict on the same data the model was trained on
# since each row in y was created by the same thetas - y_true and y_hat are the same
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
plt.close() # close last open plot
# ravel flattens X to 1D
plt.scatter(np.ravel(X),  y, label = f"real world *noisy* y intercept {reg.intercept_} coefficients {reg.coef_}")
# add line to figure, f'' and f"" are called f strings/ formatted strings in Python 3
#and are useful for printing the dynamic content of a variable together with static text content
plt.plot(np.ravel(X),  y_predicted_on_training_data, label = f'predicted y score {score }')
#show labels of lines, locate bounding box of legend above plot area
plt.legend(bbox_to_anchor=(0,1.02,1,0.2),loc="lower left",  mode = "expand")
#plt.tight_layout() #no need to use automatic layout function
plt.savefig('1b_y_real_n_predicted.png')
#######################################################################
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''
##################### 1 c  - y  is a parabula #####################

X = np.array([-2,-1, 1, 2]).reshape(-1, 1)#repeat each value in the input array twice
x_0 = X[:]
y = 1 * x_0 **2 + 4
print("y", y)
reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 1 columns/features (n=1)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)
print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# change the predicted new sample to one feature
y_predicted_new_point = reg.predict(np.array([[3]]))        # predict the value of a new point
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

y_predicted_on_training_data = reg.predict(X)  # predict on the same data the model was trained on
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
plt.close() # close last open plot
"""
add line to figure, f'' and f"" are called f strings/ formatted strings in Python 3
and are useful for printing the dynamic content of a variable together with static text content
"""
plt.scatter(X,  y, label = f"real world y intercept {reg.intercept_} coefficients {reg.coef_}") # flatten X to 1D
plt.plot(X,  y_predicted_on_training_data, label = f'predicted y score {score }')
#show labels of lines, locate bounding box of legend above plot area
plt.legend(bbox_to_anchor=(0,1.02,1,0.2),loc="lower left",  mode = "expand")
#plt.tight_layout()#makes the elements of the plot fit better - no need to use here
plt.savefig('1c_y_is_parabola.png')

