import pandas as pd

from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.metrics import balanced_accuracy_score

iris = load_iris()
##########shuffle #############
X, y = iris.data, iris.target
# Split arrays or matrices into train and test subsets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.50, shuffle=False)
model = GaussianNB()
model.fit(X_train, y_train)
# Calculate the accuracy of the model
print("Accuracy of the model:")
print("score", model.score(X_test, y_test))
y_pred = model.predict(X_test)
print("balanced_accuracy_score", balanced_accuracy_score(y_test, y_pred))

###########stratify - with and without #############
from sklearn.datasets import make_classification

# Generate and plot a synthetic imbalanced classification dataset
from collections import Counter
from sklearn.datasets import make_classification
from matplotlib import pyplot as plt
from numpy import where
import numpy as np

#################### Generate and plot a synthetic imbalanced classification dataset
from collections import Counter

# define dataset
X, y = make_classification(n_samples=10000, n_features=2, n_redundant=0,
                           n_clusters_per_class=1, weights=[0.99], flip_y=0, random_state=1)
# summarize class distribution
counter = Counter(y)
print(counter)
# scatter plot of examples by class label
for label, _ in counter.items():
    row_ix = where(y == label)[0]
    plt.scatter(X[row_ix, 0], X[row_ix, 1], label=str(label))
plt.legend()
plt.show()
############# without stratify ######
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42, test_size=0.50, shuffle=False, stratify=None)
model = GaussianNB()
model.fit(X_train, y_train)
# Calculate the accuracy of the model
print("Accuracy of the model:")
print("score", model.score(X_test, y_test))
y_pred = model.predict(X_test)
print("balanced_accuracy_score", balanced_accuracy_score(y_test, y_pred))
####### with stratify
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42, test_size=0.50, stratify=y)
test_y_ratio = len(y_test[y_test == 0]) / len(y_test[y_test == 1])
train_y_ratio = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
print("test_y_ratio", test_y_ratio)
print("train_y_ratio", train_y_ratio)
model = GaussianNB()
model.fit(X_train, y_train)
# Calculate the accuracy of the model
print("With stratify: \nAccuracy of the model:")
print("score", model.score(X_test, y_test))
y_pred = model.predict(X_test)
print("balanced_accuracy_score", balanced_accuracy_score(y_test, y_pred))

################# with kfold ####
from sklearn.model_selection import KFold
num_iterations = 5
scores = np.zeros(num_iterations)
kf = KFold(n_splits=num_iterations)
for index_split, (train_index, test_index) in enumerate(kf.split(X)):
    print("TRAIN:", train_index,  "TEST:", test_index)
    print("Train length", len(train_index),"Test length", len(test_index))
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    model = GaussianNB()  # KNeighborsClassifier(n_neighbors=7)
    model.fit(X_train, y_train)
    # Calculate the accuracy of the model
    print("Index of Split", index_split, ":\nAccuracy of the model:")
    print("score", model.score(X_test, y_test))
    y_pred = model.predict(X_test)
    scores[index_split] =  balanced_accuracy_score(y_test, y_pred)
    print("balanced_accuracy_score", scores[index_split])
print("K fold average", scores.mean())
############ stratified k fold #############
from sklearn.model_selection import StratifiedKFold
num_iterations = 5
scores_stratified = np.zeros(num_iterations)
kf = StratifiedKFold(n_splits=num_iterations)
for index_split, (train_index, test_index) in enumerate(kf.split(X, y)):
    print("TRAIN:", train_index,  "TEST:", test_index)
    print("Train length", len(train_index),"Test length", len(test_index))
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    model = GaussianNB()  # KNeighborsClassifier(n_neighbors=7)
    model.fit(X_train, y_train)
    # Calculate the accuracy of the model
    print("Index of Split - stratified k fold", index_split, ":\nAccuracy of the model:")
    print("score", model.score(X_test, y_test))
    y_pred = model.predict(X_test)
    scores_stratified[index_split] =  balanced_accuracy_score(y_test, y_pred)
    print("balanced_accuracy_score", scores_stratified[index_split])
print("stratified k fold average", scores_stratified.mean())

######## repeat stratified tt split ############
tt_strat_score = np.zeros(5)
for i in range(5):
    X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=i, test_size=0.50, stratify=y)
    test_y_ratio = len(y_test[y_test == 0]) / len(y_test[y_test == 1])
    train_y_ratio = len(y_train[y_train == 0]) / len(y_train[y_train == 1])
    print("test_y_ratio", test_y_ratio)
    print("train_y_ratio", train_y_ratio)
    model = GaussianNB()
    model.fit(X_train, y_train)
    # Calculate the accuracy of the model
    print("With stratify: \nAccuracy of the model:")
    print("score", model.score(X_test, y_test))
    y_pred = model.predict(X_test)
    tt_strat_score[i] = balanced_accuracy_score(y_test, y_pred)
    print("balanced_accuracy_score", tt_strat_score[i])
print("stratified tt split average", tt_strat_score.mean())