import seaborn as sns
def target_encoding_by_kfold(X_input,y, col_to_encode, y_col,kf, record):
    """encode category value with value of mean y col value for that category value"""
    '''
    kf = kfold_obj
    col_to_encode = 'Neighborhood'
    record = pd.DataFrame(columns = [0,1,2,3,4])
    '''
    X = pd.concat([X_input,y], axis = 1)# temp

    for index_split, (train_index, test_index) in enumerate(kf.split(X)):
        groups = X.loc[train_index].groupby(col_to_encode)
        for key,group_index in groups.groups.items(): # for example groups of different pclass
            print("X.loc[group_index,col_to_encode]", X.loc[group_index,col_to_encode])
            print("y.loc[group_index].mean()", y.loc[group_index].mean())
            X.loc[group_index,col_to_encode] = y.loc[group_index].mean() # set, for example, to the mean survival rate of that pclass
            print("After : X.loc[group_index,col_to_encode]", X.loc[group_index, col_to_encode])
            record.loc[key, index_split] = y.loc[group_index].mean()
            print("After record.loc[key, index_split]", record.loc[key, index_split])
            # record the group mean per split - this way,
            # we can take the mean of group means for the test samples which don't have the y columns
    X[col_to_encode] = X[col_to_encode].astype(int)
    _ = X.pop(y_col)
    return X,y


data = sns.load_dataset("titanic")
y = data.pop('survived')
X = data.loc[:]
num_iterations = 5 
record_neighborhood_means = pd.DataFrame(columns = np.arange(num_iterations))
target_encoded_X,y = target_encoding_by_kfold(X,y, 'pclass', y_col,kfold_obj, record_pclass_means)
