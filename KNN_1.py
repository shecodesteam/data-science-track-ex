from sklearn.neighbors import KNeighborsClassifier

a = np.array([[0.5, 5.5,1],[ 5.5, 0.5,1], [4,4,0], [3,3,0]])


X = a[:, :-1] # take all rows and all columns except the last one
y = a[:,-1] # take all rows and the last column
y = y//1 # shortcut for turning the y into an int type

knn = KNeighborsClassifier(metric = 'manhattan', n_neighbors = 1)
knn.fit(X,y)
knn.predict([[0,0]])

# Out[67]: array([1.])

knn = KNeighborsClassifier( n_neighbors = 1) # default euclidean
knn.fit(X,y)
knn.predict([[0,0]])

# Out[71]: array([0.])