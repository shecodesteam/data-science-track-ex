#https://www.kaggle.com/c/house-prices-advanced-regression-techniques

"""
General comments:

See image and csv output in the instruction slides

I chose to use the same metric as the competition:
Submissions are evaluated on Root-Mean-Squared-Error (RMSE)
between the logarithm of the predicted value and the logarithm of the observed sales price.
(Taking logs means that errors in predicting expensive houses and cheap houses will affect the result equally.)

The score is the negative of the loss - because in this way - a greater value is better which is consistent with the scoring concept

At some point I'm using KFold outside the pipeline - since I'm using the same number of folds, and the default is not to shuffle before splitting the
data into folds - I can be sure the split function returns the same indices, even without setting random_state
#https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.KFold.html?highlight=kfold#sklearn.model_selection.KFold
"""


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import OrdinalEncoder#OneHotEncoder, LabelEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import TransformedTargetRegressor, make_column_transformer, make_column_selector
from sklearn.preprocessing import QuantileTransformer
from sklearn.impute import  KNNImputer # ,SimpleImputer
from sklearn.feature_selection import SelectPercentile, SelectKBest
from sklearn.feature_selection import mutual_info_regression
import numpy as np
# from sklearn.compose import make_column_selector, make_column_transformer
#import itertools
from sklearn.model_selection import cross_val_predict
import matplotlib
from sklearn.neighbors import LocalOutlierFactor
from sklearn.model_selection import learning_curve
from sklearn.metrics import mean_squared_error, mean_absolute_error , r2_score
from sklearn.linear_model import Ridge, LinearRegression
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import KFold, train_test_split

from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline

matplotlib.rcParams.update(matplotlib.rcParamsDefault)
pd.set_option('display.max_columns', 15)
pd.set_option('display.max_rows', 20)
######## functions
def get_without_outliers(X_train, y_train = None):
    lof = LocalOutlierFactor()
    yhat = lof.fit_predict(X_train)
    # select all rows that are not outliers
    mask = yhat != -1
    if y_train is not None:
        X_train, y_train = X_train[mask, :], y_train[mask]
        return X_train, y_train
    X_train = X_train[mask, :]
    return X_train
    
def get_outlier_indices(data):
    lof = LocalOutlierFactor()
    yhat = lof.fit_predict(data)
    # select all rows that are not outliers
    mask = yhat == -1
    return mask  

'''
#use to make manual sorted list easier
for column in train.select_dtypes(include =['object' ]).columns:
    print(column)
    print( train[column].unique())
'''
def sort_by_order(list_):
    """notice which code is in the list and return sorted from low value to high,
    for some of the code lists,
    determine order of values from data description"""
    if list_.count('Gtl') >0:
        return ['Gtl','Mod', 'Sev']
    if list_.count('Alloca') >0:
        return ['Family','Abnorml','Normal' , 'Partial', 'AdjLand', 'Alloca' ] #SaleCondition
    if list_.count('GLQ') >0:
        return ['NA', 'Unf', 'LwQ' ,'Rec' ,'BLQ',  'ALQ' , 'GLQ']
    if list_.count('Ex') >0:
        return ['NA', 'Po' ,'Fa' , 'TA' ,  'Gd' ,'Ex']
    if list_.count('GdPrv') >0:
        return['NA' ,  'MnWw', 'GdWo' ,'MnPrv', 'GdPrv']
    if list_.count('P') >0:
        return['N' , 'P', 'Y']
    if list_.count('N') >0:
        return['N' ,  'Y']
    if list_.count('Pave') >0:
        return ['NA', 'Grvl', 'Pave']
    if list_.count('IR1') >0:
        return ['IR3', 'IR2', 'IR1', 'Reg']
    if list_.count('Lvl') >0:
        return ['Low', 'HLS', 'Bnk', 'Lvl']
    if list_.count('AllPub') >0:
        return ['ELO', 'NoSeWa', 'NoSeWr', 'AllPub']
    if list_.count('Inside') >0:
        return ['Inside', 'Corner', 'CulDSac', 'FR2', 'FR3']  # CulDSac=Cul-de-sac ="reasonably short street with a bulbous end"
    if list_.count('WD') >0 or list_.count('ConLI') > 0:
        return ['Oth', 'ConLD', 'ConLI', 'ConLw', 'Con', 'COD', 'New', 'CWD', 'WD']#SaleType
    if list_.count('NA') > 0:
        list_.remove('NA')
        list_.insert(0, 'NA') #move NA to the beginning of the list
        return list_
    else:
        return list_

def append_unique_categories(cat_data):
    """use for categories in ordinal encoding"""
    col_category_list  = []
    for col in cat_data.columns:
        col_category_list.append(cat_data[col].unique().tolist())
    return col_category_list

def target_encoding_by_kfold(X_input,y, col_to_encode, y_col,kf, record):
    """encode category value with value of mean y col value for that category value"""
    '''
    kf = kfold_obj
    col_to_encode = 'Neighborhood'
    record = pd.DataFrame(columns = [0,1,2,3,4])
    '''
    X = pd.concat([X_input,y], axis = 1)# temp

    for index_split, (train_index, test_index) in enumerate(kf.split(X)):
        groups = X.loc[train_index].groupby(col_to_encode)
        for key,group_index in groups.groups.items(): # for example groups of different neighborhoods
            print("X.loc[group_index,col_to_encode]", X.loc[group_index,col_to_encode])
            print("y.loc[group_index].mean()", y.loc[group_index].mean())
            X.loc[group_index,col_to_encode] = y.loc[group_index].mean() # set, for example, to the mean SalePrice of that neighborhood
            print("After : X.loc[group_index,col_to_encode]", X.loc[group_index, col_to_encode])
            record.loc[key, index_split] = y.loc[group_index].mean()
            print("After record.loc[key, index_split]", record.loc[key, index_split])
            # record the group mean per split - this way,
            # we can take the mean of group means for the test samples which don't have the y columns
    X[col_to_encode] = X[col_to_encode].astype(int)
    _ = X.pop(y_col)
    return X,y

def model_score_predict(model_or_pipeline, X,y,  model_str, experiments_record, kfold_obj, feature_importance_by = None, index_of_model =2):
    """ calculate and plot learning curve """
    # exploit_incremental_learning = True, If the estimator supports incremental learning, this will be used
    # to speed up fitting for different training set sizes.
    # error_score - if error in raised process - don't lose results, raise FitFailedWarning

    #return root mean squared error like the competition requires
    train_sizes, train_scores, test_scores, fit_times, _ = \
            learning_curve(model_or_pipeline, X, y, cv=kfold_obj, n_jobs=-1,
                           scoring = "neg_root_mean_squared_error",return_times=True, error_score =1)#"neg_mean_squared_error"

    #get the mean score of all kfolds for all
    plt.plot(train_sizes, -test_scores.mean(axis =1), 'o-', color="r",
             label="Test")
    plt.plot(train_sizes, -train_scores.mean(axis =1), 'o-', color="g",
             label="Train")
    plt.xlabel("Train size")
    plt.ylabel("Root Mean Squared Error")
    plt.title(f'Learning Curve {model_str}')
    plt.legend(loc="best")
    plt.savefig(f'{model_str}_learning_curve.png')
    plt.close()
    plt.close()
    ###### we want to also fit without the
    predicted = cross_val_predict(model_or_pipeline,X, y, cv=kfold_obj)
    plt.plot(y, predicted)

    fig, ax1 = plt.subplots(1, 1)
    ax1.scatter(y, predicted)
    ax1.plot( [y.min(), y.max()], [predicted.min(), predicted.max()], '--k') #the point on the diagonal plot are perfect predicted
    ax1.set_ylabel('Target predicted')
    ax1.set_xlabel('True Target')
    ax1.set_title(f'model_str')

    #take score from learning curve - last training set which had the most samples
    test_score = test_scores[-1].mean()#
    train_score = train_scores[-1].mean()
    root_mean_squared_error_test = mean_squared_error(y, predicted, squared=False)#returns root error

    r2_s = r2_score(y, predicted)
    pos_text_x = 0.5*(y.max() - y.min())
    pos_text_y = 0.95*(predicted.max() - predicted.min())
    ax1.text(pos_text_x, pos_text_y, rf'$R^2$={test_score:.2f}, Test RMSE={test_score:.2f}')


    experiments_record.loc[len(experiments_record), ['model_str', 'test_score', 'train_score','root_mean_squared_error' , 'r2_score' ]] = \
        model_str, test_score,train_score, root_mean_squared_error_test, r2_s
    plt.savefig(f'{model_str}_y_true_vs_y_pred.png')
    plt.close()#close the last open
    plt.close()#close the opened one before it

    ###### see which features were important
    if feature_importance_by is not None:
        model_or_pipeline.fit(X, y)
        if feature_importance_by == 'coef':
            weights = model_or_pipeline[index_of_model].coef_

            columns_with_weights = X.columns[weights > 0]
        if feature_importance_by == 'feature_importances_':
            weights = model_or_pipeline[index_of_model].feature_importances_
            importance_threshold = np.percentile(weights, 90)
            columns_with_weights = X.columns[weights > importance_threshold]
        print(model_str,":,columns_with_weights", columns_with_weights)
        return columns_with_weights
    return None

def customize_data_prep(data):
    dict_fill_values = {'MSZoning': 'RL', 'SaleType': 'Oth', 'Alley': 'NA', 'Utilities': 'AllPub',
                        'Exterior1st': 'Other', 'Exterior2nd': 'Other', 'MasVnrType': 'None', \
                        'BsmtQual': 'NA', 'BsmtCond': 'NA', 'BsmtExposure': 'NA', 'BsmtFinType1': 'NA',
                        'BsmtFinType2': 'NA', \
                        'Electrical': 'SBrkr', 'KitchenQual': 'TA', 'Functional': 'Typ', 'FireplaceQu': 'NA', \
                        'GarageType': 'NA', 'GarageFinish': 'NA', 'GarageQual': 'NA', 'GarageCond': 'NA',
                        'PoolQC': 'NA', 'Fence': 'NA', 'MiscFeature': 'NA'}

    cols_for_numerical_filling_with_0 = ['LotFrontage', 'MasVnrArea', 'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF',
                                         'TotalBsmtSF', 'BsmtFullBath', 'BsmtHalfBath', \
                                         'GarageYrBlt', 'GarageCars', 'GarageArea']


    ########### fill missing
    data[cols_for_numerical_filling_with_0] = data[cols_for_numerical_filling_with_0].fillna(0)
    data[cols_for_numerical_filling_with_0] = data[cols_for_numerical_filling_with_0].astype(int)
    data.fillna(dict_fill_values, inplace=True)# fill nans with the value matching the column key
    return data

######## initialize
train = pd.read_csv('train.csv')
test = pd.read_csv('test.csv')

scaler = StandardScaler()
encoder = OrdinalEncoder()
knn_fill = KNNImputer(n_neighbors =3)#
y_col = 'SalePrice'
palette  = sns.color_palette("tab10")
#################
####### we found that  the 'SalePrice' is not a column test df  -since the predictions on the test data are checked on submission to the competition
train.head()
test.head()
train.info()
test.info()


test_id = test['Id'].values #the id column is for the competition, save id
train_id = train['Id'].values
test.drop(columns= 'Id', inplace = True)#remove id for any other analysis
train.drop(columns= 'Id', inplace = True)


########### data preprocessing  - for plotting and modelling #################

###preprocessing for modelling
""" reading the data description, I can see that there is an integer-typed column which encodes a category,MSSubClass,
the problem is that the int values are almost in ascending order of value, but some values are out of order 
(the older houses should have aa lower value for the same sized house)
we'll switch the values so that they are all  in ascending order 
'        20	1-STORY 1946 & NEWER ALL STYLES
        30	1-STORY 1945 & OLDER
        40	1-STORY W/FINISHED ATTIC ALL AGES
        45	1-1/2 STORY - UNFINISHED ALL AGES
        50	1-1/2 STORY FINISHED ALL AGES
        60	2-STORY 1946 & NEWER
        70	2-STORY 1945 & OLDER'
"""


temp_train_ms = train['MSSubClass'].values #save original values
temp_test_ms = test['MSSubClass'].values
for val_lower,val_higher in zip([20,60], [30,70]):
    train.loc[train['MSSubClass'] == val_lower, "MSSubClass"] = val_higher
    train.loc[temp_train_ms == val_higher, "MSSubClass"] = val_lower

    test.loc[test['MSSubClass'] == val_lower, "MSSubClass"] = val_higher
    test.loc[temp_test_ms == val_higher, "MSSubClass"] = val_lower


#### concat for plotting
train_for_plot = train.copy()
train_for_plot.loc[:, 'Source'] = -1#'train'

test_for_plot = test.copy()
test_for_plot.loc[:, 'Source'] = 1#'test'

test_for_plot.loc[:,'SalePrice'] = np.nan
data = pd.concat([train_for_plot,test_for_plot], axis= 0)


####### check missing data and fill with reasonable value using data description

isna_sum = data.isna().sum()

data['MSZoning'].value_counts()#we want to see the most frequent value
'''
RL         2265
RM          460
FV          139
RH           26
C (all)      25
Name: MSZoning, dtype: int64
'''
### we could fill with 'Oth', which makes sense, because often if it is a non-standard value - the person might forget to fill it in
### or we could fill it in with the most frequent value 'WD'
### so far there is only on 'SaleType' missing value - so it doesn't seem like it will have a big impact
data['SaleType'].value_counts()
'''
WD       2525
New       239
COD        87
ConLD      26
CWD        12
ConLI       9
ConLw       8
Oth         7
Con         5
Name: SaleType, dtype: int64
'''
#### it is not data leakage to leave this step outside the pipeline -since we're using domain knowledge from the data description
#### if we wanted to put this step in the pipeline we could have used FunctionSampler
data = customize_data_prep(data)
test = customize_data_prep(test)
train = customize_data_prep(train)
######encode category columns
category_data_df = data.select_dtypes(include =['object' ]) #subset of data frame
data.loc[:,category_data_df.columns] = encoder.fit_transform(category_data_df)
#data[category_data_df.columns] = data[category_data_df.columns].astype(int)

########## scale numerical columns
numerical_data_df = data.select_dtypes(exclude=['object'])  # returns a dataframe with the selected  types
numerical_data_df.drop(columns = 'Source', inplace = True)
data.loc[:,numerical_data_df.columns] = scaler.fit_transform(numerical_data_df)
######## impute
isna_sum2 = data.isna().sum()
#only SalePrice now has nan values - fill them according to the nearest neighbors
data.loc[:] = knn_fill.fit_transform(data)

####### split data back to train and test
train_for_plot = data.loc[data['Source'] == -1]
test_for_plot = data.loc[data['Source'] == 1]
###########

select = SelectPercentile(score_func=mutual_info_regression, percentile=10)
cols = train_for_plot.columns.tolist()
cols.remove('Source')
X = train_for_plot.loc[:,cols]
y = X.pop(y_col)
new_X = select.fit_transform(X,y )
important_cols = X.columns[select.get_support()].tolist()
cols_for_plot = important_cols.copy()
cols_for_plot.append(y_col)
print("important_cols", important_cols)
'''
important_cols
Out[5]: 
Index(['Neighborhood', 'OverallQual', 'YearBuilt', 'BsmtQual', 'TotalBsmtSF',
       'GrLivArea', 'GarageCars', 'GarageArea'],
      dtype='object')
'''
categorical_mask = X.columns.isin(category_data_df.columns)
scores = mutual_info_regression(X, y,discrete_features= categorical_mask)
##########  try to see if other columns get a better mutual regression score with better ordinal encoding

col_category_list = append_unique_categories(category_data_df)
better_category_list = []
for indx,sub_list in enumerate(col_category_list):

    better_category_list.append(sort_by_order(sub_list))
    #print("ind", indx,"sub_list", sub_list, better_category_list[-1])
better_encoder = OrdinalEncoder(categories= better_category_list)

train_for_check_categories = train.copy()
train_for_check_categories.loc[:,category_data_df.columns] = better_encoder.fit_transform(train_for_check_categories[category_data_df.columns].values )

train_for_check_categories.loc[:,numerical_data_df.columns] = scaler.fit_transform(train_for_check_categories[numerical_data_df.columns])
X = train_for_check_categories.loc[:,cols]
y = X.pop(y_col)
scores_better_encoding = mutual_info_regression(X, y,discrete_features= categorical_mask)

joint_scores =pd.DataFrame(columns = X.columns)
joint_scores.loc["encoding", :] =  scores
joint_scores.loc["better_encoding", :] =  scores_better_encoding


plt.close()
with matplotlib.rc_context({'axes.labelsize': 'small'}):
    fig, ax = plt.subplots(1, 1, figsize = (30,30))
    sns.barplot(data= joint_scores, ax =ax)#, x= joint_scores.columns, orient = 'v')
    plt.xticks(rotation = 45, ha = 'right')
    plt.savefig('barplot_scores_encoding_better_encoding.png')
"""
The three best by far,  are: 
'OverallQual' - Overall quality
'Neighborhood' - which neighborhood
GrLivArea - Above grade (ground) living area square feet (not basement)

Looking at the mutual importances plot - I would not have wasted so much time in making the ordinal encoding perfect,
the differences between the encodings are small, far smaller than the difference between the features.

Next time I'll be sure to plot the scores right after calculating them.

More than half the features are categorical, including 'Neighborhood' in the second place of scores.

After checking a model with a simple ordinal encoding of Neighborhood  - I'll try target encoding
"""
print("10 top best scores with regular encoding", joint_scores.sort_values(by = 'encoding' ,axis =1, ascending =False).columns[:10])
print("10 top best scores with better ordinal encoding", joint_scores.sort_values(by = 'better_encoding' ,axis =1, ascending =False).columns[:10])
"""
The three best by far,  are: 
'OverallQual' - Overall quality
'Neighborhood' - which neighborhood
GrLivArea - Above grade (ground) living area square feet (not basement)
"""

######## plot
do_plots = False
if do_plots:
    for current_df, current_df_name in zip([ data,test_for_plot, train_for_plot],  ['all_data', 'test', 'train']):#[ test_for_plot, train_for_plot,  #[ 'test', 'train',
        ###### plot univariate distributions
        plt.close()
        ax = sns.boxenplot(data =current_df[cols_for_plot])
        print("current_df[cols_for_plot]", current_df[cols_for_plot])
        plt.savefig(f'boxenplot_important_columns_{current_df_name}_house_prices.png')
        ##############plot relationships

        plt.close()
        cols_for_balanced_train_test = cols_for_plot.copy()
        cols_for_balanced_train_test.append('Source')
        num_sources = len(current_df['Source'].unique())
        sns.pairplot(current_df[cols_for_balanced_train_test], hue = 'Source', palette= palette[:num_sources])
        plt.savefig(f'pairplot_important_columns_{current_df_name}_house_prices.png')
"""
looking at the pairplots, I can see that some features are highly correlated with each other
It makes sense that often a larger house will also have a larger garage, 
and will be built by contractors who are willing to pay for a higher overall quality.

About the neighborhoods - most neighborhoods have the approximate same range of sale price
There are a few neighborhoods which didn't have house sales at the higher prices
Two neighborhoods had a few sales at much higher prices than the common range. 

Looking at the all_data pairplot - I can see that the distribution of values is almost the same for train and test
Test has more values at 0,the mean, for SalePrice, but it was the column we imputed with KNN imputer  
"""
####### try out different models
experiments_record = pd.DataFrame(columns =['model_str', 'test_score',  'train_score'])

num_iterations = 5
train = customize_data_prep(train)
test = customize_data_prep(test)#we're going to be using this for the submission
cols = train.columns.tolist()
X = train.loc[:,cols]
y = X.pop(y_col)


column_encode = make_column_transformer(
          (OrdinalEncoder(categories= better_category_list),
            make_column_selector(dtype_include=object)), remainder = "passthrough") #the features not being transformed currently pass through as is


kfold_obj =  KFold(n_splits=num_iterations)

'''
#we're encoding the categorical columns, they are transformed to ints, 
# all columns are then scaled,  scaling the integers codes might not always be the best solution, see EdM's answer, if you wish 
https://stats.stackexchange.com/questions/399430/does-categorical-variable-need-normalization-standardization
'''
pipeline1 =  make_pipeline(column_encode, scaler,LinearRegression()) #PolynomialFeatures(degree), Ridge() #alpha=1.0, = larger values - stronger regularization


important_features_linear_regression_simple = model_score_predict(pipeline1, X,y,  'linear_regression_simple_features', experiments_record = experiments_record,\
                                         kfold_obj =kfold_obj, feature_importance_by = 'coef', index_of_model =2)
'''
the predictions are very poor, since we're using all the features - and it's confusing the linear model
let's take the 3 best ones
'''
####################
pipeline =  make_pipeline(column_encode, scaler,Ridge()) #PolynomialFeatures(degree), Ridge() #alpha=1.0, = larger values - stronger regularization


important_features_ridge_simple = model_score_predict(pipeline, X,y,  'ridge_simple_features', experiments_record = experiments_record,\
                                         kfold_obj =kfold_obj, feature_importance_by = 'coef', index_of_model =2)
"""
looking at the learning curve - we can see that the model suffers from high bias and high variance

Looking at the y vs predicted plot - we can also see more clearly the effect of outliers on the prediction.
Since this is competition data - I don't want to remove an outlier I might be required to predict - so I will apply QuantileTransform
so that the outliers are moved more to the center.

I see from the box plots that the inputs also need transformation -so the transformer is referenced twice.

The regularization is not strong, so that there is non-zero weight given to 48 columns
"""
transformer = QuantileTransformer(n_quantiles = 20,output_distribution='uniform') # output_distribution='normal'
regressor = Ridge()
regr = TransformedTargetRegressor(regressor=regressor,
                                   transformer=transformer)

pipeline_quantile_transform_input_target = make_pipeline(column_encode, scaler,transformer,regr)
model_score_predict(pipeline_quantile_transform_input_target, X,y,  'ridge_simple_features_quantile_transform_uniform',\
                    experiments_record = experiments_record, kfold_obj =kfold_obj)

"""
In the y vs predicted:
We can see that most predictions are a lot more accurate, but there are some predictions which are a lot farther from the truth then before

In the learning curves:
We can see that the variance is very low - let's help out the bias by
trying out a different quantile transformation one which transforms to "normal" distrubution
Thus, the model remains simple, but the data is easier to learn

"""
transformer2 = QuantileTransformer(n_quantiles = 20,output_distribution='normal') # output_distribution='uniform'

regr2 = TransformedTargetRegressor(regressor=regressor,
                                   transformer=transformer2)

pipeline_quantile_transform_input_target_normal = make_pipeline(column_encode, scaler,transformer2,regr2)
model_score_predict(pipeline_quantile_transform_input_target_normal, X,y,  'ridge_simple_features_quantile_transform_normal',\
                    experiments_record = experiments_record, kfold_obj =kfold_obj)

'''
both kinds of quantile transformation in the ridge pipeline are not helping - their scores are worse than the linear regression pipeline and the simple ridge

let's reduce the number of features - so that the data is easier to learn
'''
select = SelectKBest(mutual_info_regression, k =3)
pipeline_select_ridge = make_pipeline(column_encode, scaler,select,Ridge())
model_score_predict(pipeline_select_ridge, X,y,  'ridge_simple_features_w_select3',\
                    experiments_record = experiments_record, kfold_obj =kfold_obj)
"""
the r2 score is worse - the model overpredicts most points due to the outliers
the rmse is also worse than all the above, except for linear regression
let's try target encoding of 'Neighborhood'
"""
record_neighborhood_means = pd.DataFrame(columns = np.arange(num_iterations))
target_encoded_X,y = target_encoding_by_kfold(X,y, 'Neighborhood', y_col,kfold_obj, record_neighborhood_means)
column_mask= category_data_df == 'Neighborhood'
ind_neighbor =  np.where(category_data_df.columns== 'Neighborhood')[0]
# we must copy the list of lists - because it is index based, and there is one categorical column which is now a float
new_list_cats = []
for ind2,sub_list in  enumerate(better_category_list):
    if ind2 != ind_neighbor:
        new_list_cats.append(sub_list)


column_encode_new  = make_column_transformer(
          (OrdinalEncoder(categories= new_list_cats),
            make_column_selector(dtype_include=object)), remainder = "passthrough") #the features not being transformed currently pass through as is

select = SelectKBest(mutual_info_regression, k =3)
pipeline_select_ridge = make_pipeline(column_encode_new, scaler,select,Ridge())
important_cols_select_3_ridge_target_encode = model_score_predict(pipeline_select_ridge, target_encoded_X,y,  'ridge_simple_features_target_encode_select3',\
                    experiments_record = experiments_record, kfold_obj =kfold_obj)
'''
the loss and r2 score are similar to ridge with quantile transformation

looking at the learning curve, we still have a bias problem - the data is complex (with all these outliers especially) and the model is simple

we should try polynomial features (with select) 
we should try a tree-based model, where each subtree can handle different neighborhoods differently

looking at the overprediction in y vs pred, maybe repeat the target encoding experiment, but remove the outliers when calculating the mean price
'''
'''
Later comment:  Outlier sample can be removed from a copy of train, because this will not remove any Ids from the test set, and the full
submission can be made.

Might be worth check: maybe there is a feature besides Neighborhood which is usually unimportant and doesn't not have a good relationship with 
SalePrice, but this feature predicts whether the price will be an outlier.
We can add a column "Is_Outlier" to the data frame, and mark 1 where the price is an outlier,
then we can display boxenplots of all variables by Is_Outlier hue - then we can see if the distribution of 
some feature is different in the Is_Outlier case

Code:
X.loc[:, "Is_Outlier"] = 0
indices = get_outlier_indices(train[['SalePrice']])
X.loc[indices, "Is_Outlier"] = 1


plt.close()
sns.catplot(data = X, col ="Is_Outlier", kind = 'boxen')
#rotate ticks etc.
plt.savefig("boxen_by_outliers.png")  
'''

experiments_record.to_csv('experiments_record.csv')


print("record_neighborhood_means", record_neighborhood_means.describe())
'''
record_neighborhood_means                     0              1              2        3         4
count       25.000000      49.000000      73.000000     97.0      98.0
unique      25.000000      49.000000      73.000000     97.0      98.0
top     191158.825397  221059.654545  293305.555556  80000.0  112000.0
freq         1.000000       1.000000       1.000000      1.0       1.0
'''
##############

''' creates lots of plots for different column combinations
columns_for_plotting = itertools.combinations(data.columns, 6)
for column_combination in columns_for_plotting:
    plt.close()
    sns.pairplot(data[list(column_combination)])
    plt.savefig(f'column_combination_{column_combination}.png')
'''

