import math
import numpy as np
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold, train_test_split,cross_val_predict
from sklearn.metrics import log_loss
from sklearn.datasets import load_diabetes
import pandas as pd


##The sigmoid function adjusts the cost function hypotheses to adjust the algorithm proportionally for worse estimations
def sigmoid(z):
    g_of_z = float(1.0 / float((1.0 + math.exp(-1.0 * z))))
    return g_of_z

##The hypothesis is the linear combination of all the known factors x[i] and their current estimated coefficients theta[i]
##This hypothesis will be used to calculate each instance of the Cost Function
def hypothesis(theta, x):
    z = 0
    for i in range(len(theta)):
        z += x[i] * theta[i]
    return sigmoid(z)


##For each member of the dataset, the result (Y) determines which variation of the cost function is used
##The Y = 0 cost function punishes high probability estimations, and the Y = 1 it punishes low scores
##The "punishment" makes the change in the gradient of ThetaCurrent - Average(CostFunction(Dataset)) greater
### math.log is log base e (= ln)
### -(y log (p) + (1 - y) log(1 - p))
### p = the probability that it is class 1
def logistic_regression_cost_function( y,y_pred ):

    p_class_1, p_class_0 =1,0
    sum_of_errors = 0
    EPSILON = 1e-15 # we're going to add epsilon because log(0) is not defined
    m = len(X)

    for i in range(m):
        #if y[i] == 1:
        error = y[i] * math.log(y_pred[i,p_class_1] + EPSILON)
        #elif y[i] == 0:
        error += (1 - y[i]) * math.log(1 - y_pred[i, p_class_1] + EPSILON)
        sum_of_errors += error

    J = -1/m * sum_of_errors

    return J


def cross_val_predict_and_score(model, original_X, original_y, kf, method = 'predict'):
    if isinstance(original_X, (pd.DataFrame, pd.Series)):
        X = original_X.values
    else:
        X  =original_X.copy()

    if isinstance(original_y, (pd.DataFrame, pd.Series)):
        y = original_y.values
    else:
        y  =original_y.copy()

    scores = np.zeros(kf.n_splits)
    if method == 'predict':
        predictions = y.copy()
    elif method == 'predict_proba':
        num_classes = len(np.unique(y))
        predictions = np.ones((len(y), num_classes))#return 2D object, where 2nd dimension is the number of classes
    predictions[:] = -1
    for index_split, (train_index, test_index) in enumerate(kf.split(X,y)):
        X_train, y_train = X[train_index], y[train_index]
        X_test, y_test = X[test_index], y[test_index]
        model.fit(X_train, y_train)
        scores[index_split] = model.score(X_test, y_test)
        if method == 'predict':
            predictions[test_index] = model.predict(X_test)
        elif method == 'predict_proba':
            predictions[test_index] = model.predict_proba(X_test)

    # see that nothing is left as -1
    # np.where returns an array with a subarray for each dimension where the condition is met
    assert len(np.where(predictions == -1)[0]) == 0, "Not all predictions were made and/or recorded"
    return predictions, scores


############  init vars and data
rng = np.random.RandomState(1)

"""
data = load_diabetes()
print(data['DESCR']) # prints data description
"""
X, y_original = load_diabetes(return_X_y=True, as_frame=True)
X.describe()
desc_y = y_original.describe()
y = y_original[:]
y.loc[y_original < desc_y.loc['75%']] = 0
y.loc[y_original >= desc_y.loc['75%']] = 1

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=rng, stratify=y)

#############  Run models with one split
clf_nb = GaussianNB()
clf_nb.fit(X_train, y_train)
print("NB score", clf_nb.score(X_test, y_test))

clf_logistic = LogisticRegression(C=1.0)  # C  =1.0 is default, smaller values specify stronger regularization.
clf_logistic.fit(X_train, y_train)
print("LR score", clf_logistic.score(X_test, y_test))

#### test hypothesis function
pred_y = clf_logistic.predict_proba(X_test.iloc[0:1])

weights = np.concatenate([clf_logistic.intercept_,clf_logistic.coef_.ravel()], axis=0)
x_i = [1]
x_i.extend(X_test.iloc[0])
hypothesis_pred_y = hypothesis(weights,x_i)
print("LR predict automatic", pred_y, "self-calculated", hypothesis_pred_y)

clf_logistic_weak_reg = LogisticRegression(C=1e5)
clf_logistic_weak_reg.fit(X_train, y_train)
print("LR weak regularization score", clf_logistic_weak_reg.score(X_test, y_test))

clf_logistic_strong_reg = LogisticRegression(C=.02)
clf_logistic_strong_reg.fit(X_train, y_train)
print("LR strong regularization score", clf_logistic_strong_reg.score(X_test, y_test))

############## run models with 5-fold split
kf1 = StratifiedKFold(n_splits= 5)
for model, model_str in zip([clf_nb, clf_logistic, clf_logistic_weak_reg,clf_logistic_strong_reg],\
                            ["clf_nb","clf_logistic","clf_logistic_weak_reg","clf_logistic_strong_reg"]):
    predictions_probabilities, scores = cross_val_predict_and_score(model,X,y,kf1, method = 'predict_proba')
    current_log_loss = log_loss(y, predictions_probabilities)

    self_calculated_log_loss = logistic_regression_cost_function(y, predictions_probabilities )
    current_score = np.mean(scores)
    print(f"For model {model_str}, the log_loss is {current_log_loss:.2f}, the self-calculated loss is {self_calculated_log_loss:.2f}"\
          f"and the average score is {current_score}")