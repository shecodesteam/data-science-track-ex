import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
'''
Linear Regression Two Features
And More Useful Numpy Functions
'''
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''

##################### 2 a #####################

X = np.array([[1, 1], [1, 2], [2, 2], [2, 3]])#array with 4 rows and two columns: two features
x_0 = X[:, 0] #all rows, first column
x_1 = X[:, 1] #all rows, second column
y = 1 * x_0 + 2 * x_1 + 3 #one row of code - all rows of samples

reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 2 columns/features (n=2)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)

'''
since in each row  y is a linear equation of X without noise
reg.coef_ and reg.intercept are the same as the coefficients used when creating y
'''
print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# predict the value of a new point:
new_point = np.array([[1.5, 2.5]])# one sample, two features
y_predicted_new_point = reg.predict(new_point)
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

# predict on the same data the model was trained on:
y_predicted_on_training_data = reg.predict(X)
'''
since in each row  y is a linear equation of X without noise
y_true and y_hat are the same
'''
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')# 3D plot
ax.scatter(x_0, x_1, y, label = "real y")# X, Y, Z = feature 1, feature 2, y
ax.plot(x_0, x_1,  y_predicted_on_training_data, label = f'predicted y score {score} intercept {reg.intercept_} coefficients {reg.coef_}')
ax.scatter(new_point[:,0], new_point[:,1],  y_predicted_new_point, label = f'predicted unseen point')
plt.legend()# show labels of lines
plt.tight_layout()
plt.title('2A')
plt.savefig('2a_y_real_n_predicted.png')
##################### 2 b - add significant noise to y #####################

X = np.arange(0, 8).reshape((4,2))#array with numbers from 0 to 8, reshaped to 4 rows and two columns: two features
x_0 = X[:, 0] #all rows, first column
x_1 = X[:, 1] #all rows, second column
y = 1 * x_0 + 2 * x_1 + 3 #one row of code - all rows of samples
"""
y is 1D (check and see) 
# to make the noise match the shape of y, 
# multiply each by +/-5 to make the noise bigger
#y is 1D (check and see) match the shape of y, multiply each by +/-5 to make the noise bigger
"""
noise = np.random.random(4) * np.tile([-5,5],2)
y = y + noise

reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 2 columns/features (n=2)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)

print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# predict the value of a new point:
new_point = np.array([[1.5, 2.5]])# one sample, two features
y_predicted_new_point = reg.predict(new_point)
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

# predict on the same data the model was trained on:
y_predicted_on_training_data = reg.predict(X)
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')# 3D plot
ax.scatter(x_0, x_1, y, label = "real y")# X, Y, Z = feature 1, feature 2, y
#ax.plot_surface(x_0, x_1, y, alpha=0.2)
ax.plot(x_0, x_1,  y_predicted_on_training_data, label = f'predicted y score {score} intercept {reg.intercept_} coefficients {reg.coef_}')
ax.scatter(new_point[:,0], new_point[:,1],  y_predicted_new_point, label = f'predicted unseen point')
plt.legend()# show labels of lines
plt.tight_layout()
plt.title('2B')
plt.savefig('2b_y_with_noise_real_n_predicted.png', dpi = 75)
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''
##################### 2 c - y is constant #####################

X = np.arange(0, 8).reshape((4,2))#array with numbers from 0 to 8, reshaped to 4 rows and two columns: two features
x_0 = X[:, 0] #all rows, first column
x_1 = X[:, 1] #all rows, second column
y = np.ones(4) # the array has all ones, with the shape we need:flat array with length 4
reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 2 columns/features (n=2)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)

print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# predict the value of a new point:
new_point = np.array([[1.5, 2.5]])# one sample, two features
y_predicted_new_point = reg.predict(new_point)
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

# predict on the same data the model was trained on:
y_predicted_on_training_data = reg.predict(X)
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')# 3D plot
ax.scatter(x_0, x_1, y, label = "real y")# X, Y, Z = feature 1, feature 2, y
ax.plot(x_0, x_1,  y_predicted_on_training_data, label = f'predicted y score {score} intercept {reg.intercept_} coefficients {reg.coef_}')
ax.scatter(new_point[:,0], new_point[:,1],  y_predicted_new_point, label = f'predicted unseen point')
plt.legend()# show labels of lines
plt.tight_layout()
plt.title('2B')
plt.savefig('2c_y_is_constant_real_n_predicted.png')
'''
Set X to a value. (X represents a real world input).
Set y to a mapping of X. (y represents a real world output/prediction)
[For exercise 2 and on: Change X so that y is not a perfect mapping of X anymore]
Create and Train a LinearRegression object reg on (‘reg.fit(X,y)’).
Evaluate how close h(x) is to y (‘reg.score(X,y)’).
Get thetas (‘reg.intercept_’ , ‘reg.coef_’).
Predict yi of a new point xi which is not in Xi (‘reg.predict(xi)’).
Plot to visualize.
'''
##################### 2 d - y is a different constant #####################

X = np.arange(0, 8).reshape((4,2))#array with numbers from 0 to 7, reshaped to 4 rows and two columns: two features
x_0 = X[:, 0] #all rows, first column
x_1 = X[:, 1] #all rows, second column
y = np.zeros(4) # the array has all zeros, with the shape we need:flat array with length 4
reg = LinearRegression()  # reg is an object
reg.fit(X, y)  # trains the model to get the minimal value for the cost function by finding the best thetas
print("4 rows (m=4), 2 columns/features (n=2)", X)
# Evaluate how close h(x) is to y. 1 is a perfect score. The score can be negative:
score = reg.score(X, y)
print("Score", score)

print("Thetas from 1-n (hypothesis function coefficients):", reg.coef_)
print("Theta 0:", reg.intercept_)
# predict the value of a new point:
new_point = np.array([[1.5, 2.5]])# one sample, two features
y_predicted_new_point = reg.predict(new_point)
print("y_hat/y_predicted/h(x):", y_predicted_new_point)

# predict on the same data the model was trained on:
y_predicted_on_training_data = reg.predict(X)
print("Y true", y)
print("Y hat", y_predicted_on_training_data)
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')# 3D plot
ax.scatter(x_0, x_1, y, label = "real y")# X, Y, Z = feature 1, feature 2, y
ax.plot(x_0, x_1,  y_predicted_on_training_data, label = f'predicted y score {score} intercept {reg.intercept_} coefficients {reg.coef_}')
ax.scatter(new_point[:,0], new_point[:,1],  y_predicted_new_point, label = f'predicted unseen point')
plt.legend()# show labels of lines
plt.tight_layout()
plt.title('2B')
plt.savefig('2d_y_is_a_different_constant_real_n_predicted.png')